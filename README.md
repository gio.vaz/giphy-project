



# giphy-project

# Giphy Project

Proyecto para KTBO test front-developer.

Consumir el API de Giphy, y mostrar los resultados de la busqueda.

Tambien mostrar el top ten de los gif's.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
